<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Share what you want</h3>
  </div>
  <div class="panel-body">
    <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>" id="uploader" enctype="multipart/form-data">
        <div class="form-group">
            <div id="preview"></div>
        </div>
        <div class="form-group">
             <label>Share an image</label>
            <input type="file" class="btn btn-primary"  name="fileUpload" id="fileUpload">
        </div>
        <div class="form-group">
               <label>Share a video</label>
            <input type="file" class="btn btn-primary" name="videoUpload" id="videoUpload">
        </div>
        <div class="form-group">
            <label>Share Title</label>
            <input type="text" name="title" class="form-control" />
        </div>
        <div class="form-group">
            <label>Body</label>
            <textarea name="body" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label>Link</label>
            <input type="text" name="link" class="form-control" />
        </div>
        <input class="btn btn-primary" name="submit" type="submit" value="Submit" />
        <a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>shares">Cancel</a>
    </form>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
      $(document).ready(function () {
            $('#fileUpload').change(function () {
                console.dir(this.files[0]);
                var myImage = new FileReader();
                myImage.onload = imageReady;
                myImage.readAsDataURL(this.files[0]);
            })
        })

        function imageReady(e) {
            $('#preview').html('<img src="' + e.target.result + '" width="200px">');
            $.ajax({

                url: "../views/shares/save.php"
                , type: "POST"
                , data: {
                    baseData: e.target.result
                }
                , dataType: "text"
                , success: function (data) {
                    console.log(data);
                  //  $('#preview').html('<img src="' + data + '" width="200px">');
                }
                , error: function (xhr) {
                    console.log(xhr);
                    console.log("Does not go to url");
                }
            })

            console.log(e);
        }
    </script>
  </div>
</div>
