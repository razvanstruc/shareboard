<html>
<head>
  <title>Shareboard</title>
  <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/style.css">
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

</head>
<body>
  <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo ROOT_URL; ?>">Shareboard</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <div class="nav navbar-nav">
            <li><a href="<?php echo ROOT_URL; ?>">Home</a></li>
            <li><a href="<?php echo ROOT_URL; ?>shares">Shares</a></li>
          </div>

          <div class="nav navbar-nav navbar-center">
            <?php if(isset($_SESSION['is_logged_in'])) : ?>
            <li><a href="<?php echo ROOT_URL; ?>">Welcome <?php echo $_SESSION['user_data']['name']; ?></a></li>
            <li><a href="<?php echo ROOT_URL; ?>users/logout">Logout</a></li>
          <?php else : ?>
            <li><a href="<?php echo ROOT_URL; ?>users/login">Login</a></li>
            <li><a href="<?php echo ROOT_URL; ?>users/register">Register</a></li>
          <?php endif; ?>
          </div>
        </div><!--/.nav-collapse -->
      </div>

    </nav>


    
    <div class="container">

     <div class="row">
      <?php Messages::display(); ?>
      <?php 
      echo $view;
      if ( $view)
      require(strtolower($view)); ?>
     </div>

    </div><!-- /.container -->

 

       <div class="footer">

<?php include("Chat/index.php");?>
</div>
</body>
</html>