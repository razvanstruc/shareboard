
var me = {};
me.avatar = "https://d30y9cdsu7xlg0.cloudfront.net/png/17241-200.png";

var you = {};
you.avatar = "http://icons.iconarchive.com/icons/paomedia/small-n-flat/512/user-male-icon.png";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

//-- No use time. It is a javaScript effect.
function insertChat(who, text, dateTime,user){
    //TODO DATETIME
    var date = formatAMPM(new Date());
    if (who == "me"){
        control = '<li style="width:100%">' +
                        '<div class="msj macro">' +
                        '<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ me.avatar +'" /></div>' +
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small>'+user+" "+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:100%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small>'+user+" "+date+'</small></p>' +
                            '</div>' +
                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="'+you.avatar+'" /></div>' +                                
                  '</li>';
    }


      $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
    
}

function resetChat(){
    $("ul").empty();
}

$(".mytext").on("keydown", function(e){
    if (e.which == 13){
        var text = $(this).val();
        if (text !== ""){
            insertChat("me", text);              
            $(this).val('');
        }
    }
});

$('body > div > div > div:nth-child(2) > span').click(function(){
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
})

//-- Clear Chat

function reply_click(clicked_id)
{

    var text = document.getElementById("myText").value;

      $.ajax({
            //to change to relative
                url: "http://struc-dsyf87hsai23.avenocam.com/chat/add"
                , type: "POST"
                , data: {
                    baseData: text
                }
                , dataType: "text"
                , success: function (data) {
                    //console.log(data);
                    //console.log("success");

                }
                , error: function (xhr) {
                    //console.log(xhr);
                    //.log("Does not go to url");
                }
            })
      resetChat();
    getdata();

}


resetChat();
getdata();

var last_id;
 function getdata() {

    $.ajax({
            //to change to relative
               // url: "http://localhost/shareboard/chat_controller/chat_getall.php"
                url: "http://struc-dsyf87hsai23.avenocam.com/chat/getAll"
                , type: "POST"
                ,dataType: 'json'
                 ,data: {mydata: ''}  
                , success: function (data) {
                    if ( last_id != data[0][data[0].length-1].id ) {
                        resetChat();
                         $.each(data[0], function () {
                                                if(data[1] == this.user_email) {
                                                  //   console.log(this.message);
                                                     insertChat("you",this.message,this.date,this.user_email);
                                                   // console.log(this.message);

                                                }
                                                else {
                                                     insertChat("me",this.message,this.date,this.user_email);

                                                }
                                            });

                                             last_id = data[0][data[0].length-1].id;
                        }
  
                    
                }
                , error: function (xhr) {
                    console.log(xhr);
                    console.log("Does not go to url");
                }
            })

}

function loadlink(){
         getdata();
}

setInterval(function(){
    loadlink()
   // this will run after every 0.5 seconds
}, 100);


//-- Print Messages
//-- NOTE: No use time on insertChat.