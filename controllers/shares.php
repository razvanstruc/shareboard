<?php
class Shares extends Controller {
	protected function Index() {
		$viewmodel = new ShareModel();
		$this->ReturnView($viewmodel->Index(), true);
	}
	protected function add() {
		if( !isset($_SESSION['is_logged_in']) ) {
			header('Location: '.ROOT_URL.'shares');
		}

		$viewmodel = new ShareModel();
		$this->ReturnView($viewmodel->add(), true);
	}
	protected function get_image(){
		// gets the imege
		if( isset($_SESSION['is_logged_in']) ) {
			$item = $_GET['item'];
			header("Content-Type: image/jpeg");
			imagejpeg(imagecreatefromjpeg(dirname(__FILE__,2)."\images\private/".$item.".jpeg"));
		
		}
	}
}