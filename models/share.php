<?php
class ShareModel extends Model {
	public function Index() {
		$this->query('SELECT * FROM share ORDER BY create_date DESC');
		$rows = $this->resultSet();
		return $rows;
	}
	public function add() {
		// Sanitize POST


		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		if($post['submit'] && getimagesize($_FILES['fileUpload']["tmp_name"]) ) {
			
			$name = $_FILES['videoUpload']['name'];
			$extension = explode('.', $name);
			$extension = end($extension);
			$type = $_FILES['videoUpload']['type'];
			$size = $_FILES['videoUpload']['size'] /1024/1024;
			$random_name = rand();
			$tmp = $_FILES['videoUpload']['tmp_name'];
			
			
			if ((strtolower($type) != "video/mpg") && (strtolower($type) != "video/wma") && (strtolower($type) != "video/mov") 
			&& (strtolower($type) != "video/flv") && (strtolower($type) != "video/mp4") && (strtolower($type) != "video/avi") 
			&& (strtolower($type) != "video/qt") && (strtolower($type) != "video/wmv") && (strtolower($type) != "video/wmv"))
			{
				$message= "Video Format Not Supported !";

			}else
			{
				move_uploaded_file($tmp, dirname(__FILE__,2) . '/videos/'.$random_name.'.'.$extension);	
				//$conn->query("insert into videos (title,location) values ('$name','$random_name.$extension')");
				$message="Video Uploaded Successfully!";
			

				// $ffmpeg = FFMpeg\FFMpeg::create();
				// $video = $ffmpeg->open(dirname(__FILE__,2) . '/videos/'.$random_name.'.'.$extension);

				// $video->filters()->clip(FFMpeg\Coordinate\TimeCode::fromSeconds(0), FFMpeg\Coordinate\TimeCode::fromSeconds(5));

				// $video->save(new FFMpeg\Format\Video\X264(), 'output.mp4');


				//Copy normal image
	            $img = $_FILES['fileUpload']['tmp_name'];
				$imgContent = addslashes(file_get_contents($img));
				$imageName = 'shareImage'.rand(1,10000000);
	            //TODO FILE EXISTANCE
				$imagepath = dirname(__FILE__,2) . '\images\private/'.$imageName.'.jpeg';
				move_uploaded_file($img, $imagepath);

				//Create blurred image
			    $blurredImageName = 'shareImage'.rand(1,10000000);
	            $image = imagecreatefromjpeg($imagepath);
	               //two metods to do it GD Power
	               // $gaussian = array(array(1.0, 2.0, 1.0), array(2.0, 4.0, 2.0), array(1.0, 2.0, 1.0));
	               // imageconvolution($image, $gaussian, 16, 0);
	            for ($x=1; $x<=25; $x++)
	                imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
	            $imagepath = dirname(__FILE__,2) . '/images/'.$blurredImageName.'.jpeg';
	            imagejpeg($image, $imagepath);
			

				// Insert into database
				$this->query('INSERT INTO share (title, body, link, user_id, normalimage, blurredimage, normalvideo) VALUES (:title, :body, :link, :user_id, :normalimage, :blurredimage, :normalvideo)');
				$this->bind(':title', $post['title']);
				$this->bind(':body', $post['body']);
				$this->bind(':link', $post['link']);
				$this->bind(':user_id', 1);
				$this->bind(':normalimage', $imageName);
				$this->bind(':blurredimage', $blurredImageName);
				$this->bind(':normalvideo', $random_name.".".$extension );
				$this->execute();
				// check
				if($this->lastInsertId()){
					// Redirect
					header('Location: '.ROOT_URL.'shares');
				}
			}
			return;
	}
	}
      
}
